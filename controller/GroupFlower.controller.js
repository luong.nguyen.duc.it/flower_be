const { GroupFlower } = require('../models')

const AddGroup = async (req, res) => {
    const { GroupName } = req.body;
    try {
        const checkGroupName = await GroupFlower.findOne({ where: { GroupName } });
        if (!checkGroupName) { // nếu mà tên nhóm hoa chưa tồn tại
            if (GroupName) { // kiểm tra xem có nhập GroupName hay ko ?
                const newGroup = await GroupFlower.create({ GroupName })
                res.status(200).send(newGroup)
            } else {
                res.status(403).send("Data is not enough")
            }
        }
        else {
            res.status(403).send("GroupName is exists")
        }
    } catch (error) {
        res.status(500).send(error)
    }
}
const AllGroupFlower = async (req, res) => {
    try {
        const listGroupFlower = await GroupFlower.findAll({ where: { IsActive: true } })
        res.status(200).send(listGroupFlower)
    } catch (error) {
        res.status(500).send(error)
    }
}
const DetailGroupFlower = async (req, res) => {
    const { detail } = req;
    try {
        if (detail) {
            res.status(200).send(detail)
        } else {
            res.status(404).send("GroupFlower is not exists")
        }
    } catch (error) {
        res.status(500).send(error)
    }
}
const UpdateGroupFlower = async (req, res) => {
    const { GroupName } = req.body;
    const { detail } = req;
    try {
        if (GroupName) {
            if (detail) {
                detail.GroupName = GroupName;
                await detail.save();
                res.status(200).send(detail)
            } else {
                res.status(404).send("GroupFlower is not exists")
            }
        } else {
            res.status(403).send("Data is not enough")
        }
    } catch (error) {
        res.status(500).send(error)
    }
}

const DeleteGroupFlower = async (req, res) => {
    const { detail } = req;
    try {
        if (detail) {
            detail.IsActive = false;
            await detail.save();
            res.status(200).send(detail)
        } else {
            res.status(404).send("GroupFlower is not exists")
        }
    } catch (error) {
        res.status(500).send(error)
    }
}

module.exports = {
    AddGroup,
    AllGroupFlower,
    DetailGroupFlower,
    UpdateGroupFlower,
    DeleteGroupFlower
}
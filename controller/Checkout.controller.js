const { Checkout, DetailCheckout, Account, Product, GroupFlower } = require('../models')
const Func_Checkout = async (req, res) => {
    const { Account_ID, ListProduct } = req.body;
    try {
        if (Account_ID && ListProduct.length != 0) {
            console.log(ListProduct)
            let total = 0;
            let checkoutResult = {}
            ListProduct.forEach(product => {
                total += (product.Price * product.Quantity) - (product.Price * product.Quantity * product.Discount / 100);
            });
            const _checkout = await Checkout.create({
                Account_ID, TotalMoney: total
            }).then(async (data) => {
                checkoutResult.Checkout = data;
                checkoutResult.OrderProduct = [];
                for (const product of ListProduct) {
                    console.log(product)
                    const detailCheckout = await DetailCheckout.create({
                        Product_ID: product.Product_ID,
                        Price: product.Price,
                        Quantity: product.Quantity,
                        Checkout_ID: data.id
                    })
                    await checkoutResult.OrderProduct.push(detailCheckout)
                }
                res.status(200).send(checkoutResult)
            })
        } else {
            if (!Account_ID) {
                res.status(400).send("You can login");
            } else if (!ListProduct) {
                res.status(400).send("You can choose Product");
            } else {
                res.status(403).send("Checkout fail");
            }
        }
    } catch (error) {
        res.status(500).send(error)
    }
}

const AllBill = async (req, res) => {
    try {
        const lstBill = await Checkout.findAll({
            include: [
                {
                    model: Account,
                    as: 'Account'
                }
            ]
        });
        res.status(200).send(lstBill)
    } catch (error) {
        res.status(500).send(error)
    }
}
const FindBill = async (req, res) => {
    const { id } = req.params;
    try {
        if (id) {
            const billDetail = await Checkout.findOne({
                where: {
                    id
                },
                include: [
                    {
                        model: Account,
                        as: 'Account',
                        attributes: ['Username']
                    }
                ],
                attributes: {
                    exclude: ['createdAt', 'Account_ID']
                },
            });
            if (billDetail) {
                res.status(200).send(billDetail)
            } else {
                res.status(403).send(`BILL ${id} NOT FOUND`)
            }
        }
    } catch (error) {
        res.status(500).send(error)
    }
}
const GetBillByID_User = async (req, res) => {
    const { id } = req.params;
    try {
        Checkout.findAll({
            where: { Account_ID: id },
            attributes: {
                exclude: ["Account_ID"]
            }
        }).then(async (lstBill) => {
            let arrBill = []
            for (const bill of lstBill) {
                const detailBill = await DetailCheckout.findAll({
                    where: {
                        Checkout_ID: bill.id
                    },
                    include: [
                        {
                            model: Product,
                            as: 'Product',
                            attributes: ['ProductName', 'Price', 'Discount', "ProductImage"],
                            include: [
                                {
                                    model: GroupFlower,
                                    as: 'GroupFlower',
                                    attributes: ['GroupName']
                                },
                            ]
                        }
                    ],
                    attributes: {
                        exclude: ['createdAt', 'updatedAt', 'Product_ID', "Checkout_ID"]
                    },
                })
                // console.log("detailBill", detailBill)
                if (detailBill) {
                    const _bill = {
                        Bill: bill,
                        Detail: detailBill
                    }
                    arrBill = [...arrBill, _bill]
                }

            }
            res.status(200).send(arrBill)
        })

    } catch (error) {
        res.status(500).send(error)
    }
}
const FindDetailBill = async (req, res) => {
    const { id } = req.params;
    try {
        const billDetail = await DetailCheckout.findAll({
            where: {
                Checkout_ID: id
            },
            include: [
                {
                    model: Product,
                    as: 'Product',
                    attributes: ['ProductName', 'Price', 'Discount'],
                    include: [
                        {
                            model: GroupFlower,
                            as: 'GroupFlower',
                            attributes: ['GroupName']
                        },
                    ]
                }
            ],
            attributes: {
                exclude: ['createdAt', 'updatedAt', 'Product_ID']
            },
        })
        res.status(200).send(billDetail)
    } catch (error) {
        res.status(500).send(error)
    }
}
module.exports = {
    Func_Checkout,
    AllBill,
    FindBill,
    FindDetailBill,
    GetBillByID_User
}
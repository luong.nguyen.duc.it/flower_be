const express = require('express');
const { GroupFlower } = require('../models')

const { AddGroup, AllGroupFlower, DetailGroupFlower, UpdateGroupFlower, DeleteGroupFlower } = require('../controller/GroupFlower.controller');
const { CheckExists } = require('../middleware/validations/CheckExists');


const groupFlowerRouter = express.Router();

groupFlowerRouter.post('/', AddGroup)
groupFlowerRouter.put('/:id', CheckExists(GroupFlower), UpdateGroupFlower)
groupFlowerRouter.put('/Delete/:id', CheckExists(GroupFlower), DeleteGroupFlower)
groupFlowerRouter.get('/', AllGroupFlower)
groupFlowerRouter.get('/:id', CheckExists(GroupFlower), DetailGroupFlower)
module.exports = {
    groupFlowerRouter
}
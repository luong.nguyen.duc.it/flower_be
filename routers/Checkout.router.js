const express = require('express');
const { Func_Checkout, AllBill, FindBill, FindDetailBill, GetBillByID_User } = require('../controller/Checkout.controller');
const { CheckExists } = require('../middleware/validations/CheckExists');
const { Checkout, DetailCheckout } = require('../models')

const checkoutRouter = express.Router();

checkoutRouter.post('/', Func_Checkout)
checkoutRouter.get('/ByAccount/:id', GetBillByID_User)
checkoutRouter.get('/', AllBill)
checkoutRouter.get('/:id', FindBill)

checkoutRouter.get('/Detail/:id', FindDetailBill)
module.exports = {
    checkoutRouter
}
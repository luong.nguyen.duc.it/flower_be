const express = require('express');
const { accountRouter } = require('./Account.router');
const { checkoutRouter } = require('./Checkout.router');
const { groupFlowerRouter } = require('./GroupFlower.router');
const { productRouter } = require('./Product.router');


const rootRouter = express.Router();
rootRouter.use('/Account', accountRouter)
rootRouter.use('/GroupFlower', groupFlowerRouter)
rootRouter.use('/Product', productRouter)
rootRouter.use('/Checkout', checkoutRouter)
module.exports = {
    rootRouter
}
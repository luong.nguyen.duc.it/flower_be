'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ProductName: {
        allowNull: false,
        type: Sequelize.STRING
      },
      ProductImage: {
        allowNull: false,
        type: Sequelize.STRING
      },
      Description: {
        allowNull: false,
        type: Sequelize.STRING
      },
      Price: {
        allowNull: false,
        type: Sequelize.STRING
      },
      Discount: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      IsActive: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      GroupFlower_ID: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'GroupFlowers',
          key: 'id'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Products');
  }
};